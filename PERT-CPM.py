import sys, re, os, string, re
from PyQt5.QtWidgets import *
from PyQt5 import *
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt
import numpy as np
from datetime import date, datetime, timedelta
from src.view.principal import Ui_Principal
from src.view.fecha import Ui_Dialog
from src.view.ventana import Ui_ventana
import tkinter as tk
from tkinter import messagebox
from tkinter import *
from tkinter import ttk
from Tabla  import Tabla
from especiales import CEspeciales
from Matriz_sig import Matriz



try:
    from pandas import DataFrame
    pandas_av = True
except ImportError:
    pandas_av = False
    pass


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_Principal()
        self.ui.setupUi(self)
        self.perl = Perl(self.ui)
        self.ui.actionSimplex.triggered.connect(self.showSimplexUI)
        self.ui.actionPerl.triggered.connect(self.showPerlUI)

    def showSimplexUI(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.pageSimplex)
        self.perl = Simplex(self.ui)
    
    def showPerlUI(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.pagePerl)
        self.perl = Perl(self.ui)

       
class Perl(QMainWindow):
    def __init__(self, ui):
         super(Perl, self).__init__()
         self.ui = ui
 
         self.ui.btnGenera.clicked.connect(self.genera)
         self.ui.btnNuevo.clicked.connect(self.limpia)
         self.ui.btnCalcular.clicked.connect(self.calculo)
         self.ui.btnCalcular.setEnabled(False)

        
         self.fechInicio = ""
         for indice, ancho in enumerate((60, 100, 80, 30, 30, 30), start=0):
             self.ui.tabla.setColumnWidth(indice, ancho)
         
         for indice, ancho in enumerate((35, 35, 30, 30, 30, 30, 30,30, 125,110,120,100), start=0):
             self.ui.tablaView.setColumnWidth(indice, ancho)
         self.ui.tablaView.setEditTriggers(QAbstractItemView.NoEditTriggers)

    def genera(self):
        self.ui.btnCalcular.setEnabled(True)
        self.variables = self.ui.canVariable.value()
        self.ui.tabla.verticalHeader().setDefaultSectionSize(20)
        self.ui.tabla.verticalHeader().setVisible(False)
        self.ui.tablaView.verticalHeader().setDefaultSectionSize(20)
        self.ui.tablaView.verticalHeader().setVisible(False)
        
        self.Actividades = []
        if(self.variables > 16):
            QMessageBox.information(None, ('Error'), ('El maximo de las variables es de 16'))
        else:
            self.ui.tabla.setRowCount(self.variables)
            self.ui.tablaView.setRowCount(self.variables)
            self.abec = list(map(chr, range(65, 91)))
            for i in range(self.variables):
                self.Actividades.append(self.abec[i])
                celda = QTableWidgetItem(self.abec[i])
                celda.setTextAlignment(Qt.AlignHCenter)
                self.ui.tabla.setItem(i, 0, celda)
                self.ui.tabla.setItem(i,2, QTableWidgetItem(" "))
                self.ui.tabla.setItem(i,1, QTableWidgetItem(" "))
            
            
        ##
    def calculo(self):
        try:
            self.Predecesores = []
        
            """porfavor """
            for f in range(self.variables):
                valor = self.ui.tabla.item(f,2).text()
                self.Predecesores.append(valor)
                regex = re.search(r" |^[A-Z]{1}$|^[A-Z]{1}(,[A-Z]{1}){1,10}$", valor)
                if(regex == None):
                    raise Exception('Solo admite letras mayusculas y separadas por , \nEj: A  / A,B')

                prede = regex.group()
                listPrede = prede.split(sep=",")
                for value in range(len(listPrede)):
                    if(listPrede[value] != " " and not(listPrede[value] in self.Actividades)):
                        raise Exception(f'El predecesor "{listPrede[value]}" no corresponde a ninguna actividad existente')

                for j in range(3):
                    if self.ui.tabla.item(f,j+3) == None:
                        raise Exception('Revisar si falta un valor.\nEn: To, Tn, Tp')
        
            self.dialog = Dialog()
            self.dialog.show()
            self.DijOij = self.calculaDijOij(self.variables)
            self.calculaTiempos(self.variables)
            self.dialog.ui.btnFecha.clicked.connect(self.obtenerFecha)
            
            
        except Exception as err:
            
            QMessageBox.information(None, ('Error'), str(err))

   

    def calculaDijOij(self, filas):
        try:
            dij = []
            oij = []
            for f in range(filas):
                valores = []
                for c in range(3):
                    valor = int(self.ui.tabla.item(f,c+3).text())
                    valores.append(valor)
                
                valorDij = (valores[0] + (4 * valores[1]) + valores[2]) / 6
                valorDij = round(valorDij)
                dij.append(valorDij)
                
                valorOij = pow(((valores[2] - valores[0]) / 6), 2)
                valorOij = round(valorOij, 2)
                oij.append(valorOij)
                
                celdaDij = QTableWidgetItem(str(valorDij))
                celdaDij.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 0, celdaDij)
                
                celdaOij = QTableWidgetItem(str(valorOij))
                celdaOij.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 1, celdaOij)
                
            return dij, oij
        except Exception as err:
            QMessageBox.information(None, ('Error'), str(err))
    

    def calculaTiempos(self, filas):

        self.Ti0 = []
        self.Tj0 = []
        self.Ti1 = []
        self.Tj1 = []
        for f in range(filas):
            validarPrece = []
            valorPrece = self.ui.tabla.item(f,2).text()
            
            if(valorPrece == " "):
                celdaTi0 = QTableWidgetItem(str(0))
                celdaTi0.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 2, celdaTi0)
                self.Ti0.append(0)
                
                celdaTj0 = QTableWidgetItem(str(self.DijOij[0][f]))
                celdaTj0.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 4, celdaTj0)
                self.Tj0.append(self.DijOij[0][f])
                
            else:
                listPrede = valorPrece.split(sep=",")
                for value in range(len(listPrede)):
                    indexFilaTc = self.Actividades.index(listPrede[value])
                    valorPreceTc = int(self.ui.tablaView.item(indexFilaTc,4).text())
                    validarPrece.append(valorPreceTc)
                
                valorMax = max(validarPrece)
                celdaTi0 = QTableWidgetItem(str(valorMax))
                celdaTi0.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 2, celdaTi0)
                self.Ti0.append(valorMax)
                
                celdaTj0 = QTableWidgetItem(str(valorMax + self.DijOij[0][f]))
                celdaTj0.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(f, 4, celdaTj0)
                self.Tj0.append(valorMax + self.DijOij[0][f])
                
        lastFilaAct = len(self.Actividades)-1
        validarLastAct = []
        for i in range(lastFilaAct, -1, -1):
            validarPreceLast = []
            valorPreceLast = self.ui.tabla.item(i,2).text()
            if(len(self.Actividades) == i+1):
                vMax = max(self.Tj0)
                celdaTj1 = QTableWidgetItem(str(vMax))
                celdaTj1.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(i, 5, celdaTj1)
                self.Tj1.append(vMax)
                
                celdaTi1 = QTableWidgetItem(str(vMax - self.DijOij[0][i]))
                celdaTi1.setTextAlignment(Qt.AlignCenter)
                self.ui.tablaView.setItem(i, 3, celdaTi1)
                self.Ti1.append(vMax - self.DijOij[0][i])
                
                listPredeLast = valorPreceLast.split(sep=",")
                validarLastAct.append(listPredeLast)
            else:
                listPredeLast = valorPreceLast.split(sep=",")
                validarLastAct.append(listPredeLast)
                indexFilaLast = []
                for value in range(len(validarLastAct)):
                    if(self.Actividades[i] in validarLastAct[value]):
                        predecesor = ",".join(validarLastAct[value])
                        index = [indice for indice in range(len(self.Predecesores)) if self.Predecesores[indice] == predecesor]
                        
                        for ind in index:
                            if(not(ind in indexFilaLast)):
                                indexFilaLast.append(ind)
                
                if(len(indexFilaLast) == 0):
                    valorTj0 = int(self.ui.tablaView.item(i+1, 5).text())
                    celdaTj1 = QTableWidgetItem(str(valorTj0))
                    celdaTj1.setTextAlignment(Qt.AlignCenter)
                    self.ui.tablaView.setItem(i, 5, celdaTj1)
                    self.Tj1.append(valorTj0)
                    
                    celdaTi1 = QTableWidgetItem(str(valorTj0 - self.DijOij[0][i]))
                    celdaTi1.setTextAlignment(Qt.AlignCenter)
                    self.ui.tablaView.setItem(i, 3, celdaTi1)
                    self.Ti1.append(valorTj0 - self.DijOij[0][i])
                else:
                    valorPreceMin = []
                    for valor in indexFilaLast:
                        valorTj0 = int(self.ui.tablaView.item(valor,3).text())
                        valorPreceMin.append(valorTj0)
                    
                    valorMin = min(valorPreceMin)
                    celdaTj1 = QTableWidgetItem(str(valorMin))
                    celdaTj1.setTextAlignment(Qt.AlignCenter)
                    self.ui.tablaView.setItem(i, 5, celdaTj1)
                    self.Tj1.append(valorMin)
                    
                    celdaTi1 = QTableWidgetItem(str(valorMin - self.DijOij[0][i]))
                    celdaTi1.setTextAlignment(Qt.AlignCenter)
                    self.ui.tablaView.setItem(i, 3, celdaTi1)
                    self.Ti1.append(valorMin - self.DijOij[0][i])
                    
        self.calcularMtMl(filas)
    
    def calcularMtMl(self, filas):
        self.MTij = []
        self.MLij = []
        tj1 = list(reversed(self.Tj1))
        ti0 = self.Ti0
        tj0 = self.Tj0
        dij = self.DijOij[0]
        
        for f in range(filas):
            mtij = (tj1[f] - ti0[f] - dij[f])
            celdaMTij = QTableWidgetItem(str(mtij))
            celdaMTij.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f, 6, celdaMTij)
            self.MTij.append(mtij)
            
            mlij = (tj0[f] - ti0[f] - dij[f])
            celdaMLij = QTableWidgetItem(str(mlij))
            celdaMLij.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f, 7, celdaMLij)
            self.MLij.append(mlij)
        

    def getDiasNoLab(self):
        self.diasNoLab = []
        
        if(self.dialog.ui.boxLunes.isChecked()):
            dia = "Monday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.boxMartes.isChecked()):
            dia = "Tuesday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.boxMiercoles.isChecked()):
            dia = "Wednesday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.boxJueves.isChecked()):
            dia = "Thursday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.boxViernes.isChecked()):
            dia = "Friday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.boxSabado.isChecked()):
            dia = "Saturday"
            self.diasNoLab.append(dia)
            
        if(self.dialog.ui.BoxDomingo.isChecked()):
            dia = "Sunday"
            self.diasNoLab.append(dia)
       
    def obtenerFecha(self):
        self.fechInicio = self.dialog.ui.Date.selectedDate().toPyDate()
        self.getDiasNoLab()
        self.generateDate(self.variables)

    def calcuFecha(self, tiempo):
        self.fechaLab = self.fechInicio
        i = 0
        while(i < tiempo):
            if(i == 0):
                dia = self.fechInicio + timedelta(days=i)
            else:
                self.fechaLab = self.fechaLab + timedelta(days=1)
                diaActual = self.fechaLab.strftime("%A")
                if(diaActual in self.diasNoLab):
                    i -= 1
            i += 1

        return self.fechaLab
            
        
    def generateDate(self, filas):
        
        self.Ti1 = list(reversed(self.Ti1))
        self.Tj1 = list(reversed(self.Tj1))
        for f in range(filas):
            
            ti0 = self.Ti0[f]
            ti1 = self.Ti1[f]
            tj0 = self.Tj0[f]
            tj1 = self.Tj1[f]

            # Fecha Inicio Temprano
            fi0 = self.calcuFecha(ti0)
            diaStr = QTableWidgetItem(fi0.strftime("%d/%m/%Y"))
            diaStr.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f,8,diaStr)
            
            # Fecha Inicio Tardio
            fi1 = self.calcuFecha(ti1)
            diaStr = QTableWidgetItem(fi1.strftime("%d/%m/%Y"))
            diaStr.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f,9,diaStr)
            
            # Fecha Fin Temprano
            fj0 = self.calcuFecha(tj0)
            diaStr = QTableWidgetItem(fj0.strftime("%d/%m/%Y"))
            diaStr.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f,10,diaStr)
            
            # Fecha Fin Tardio
            fj1 = self.calcuFecha(tj1)
            diaStr = QTableWidgetItem(fj1.strftime("%d/%m/%Y"))
            diaStr.setTextAlignment(Qt.AlignCenter)
            self.ui.tablaView.setItem(f,11,diaStr)
            
        # Búcle: colorea las actividades críticas
        rutaCritica = [index for index in range(len(self.MTij)) if self.MTij[index] == 0]
        
        for ruta in rutaCritica:
            for g in range(6):
                self.ui.tabla.item(ruta,g).setBackground(QtGui.QColor(255, 60, 60))
            for k in range(12):
                self.ui.tablaView.item(ruta,k).setBackground(QtGui.QColor(255, 60, 60))
            
    

    #limpiar la tabla
    def limpia(self):
        self.ui.btnCalcular.setEnabled(False)
        self.ui.tabla.clearContents()
        self.ui.tabla.setRowCount(0)
        self.ui.tablaView.clearContents()
        self.ui.tablaView.setRowCount(0)

class Dialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(Dialog, self).__init__(*args, **kwargs)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)  
        self.setWindowTitle("Fecha")
        self.ui.btnFecha.clicked.connect(self.close)



class Simplex(QMainWindow):
    def __init__(self, ui):
        super(Simplex, self).__init__()
        self.ui = ui
        self.window = tk.Tk()
        self.ventana1()
        self.numvariables = None
        self.numvarestricciones = None

    def userText(self,even):
        self.numvariables.delete(0, tk.END)
        self.numvariables.config(fg="#ecf0f1")

    def userText1(self, even):
        self.numvarestricciones.delete(0, tk.END)
        self.numvarestricciones.config(fg="#ecf0f1")

    def ventana1(self):
        self.window.title("Metodo simplex")
        self.window.resizable(0,0)  #No redimension
        inicio = tk.Frame() #el frame
        inicio.pack(fill="both", expand="true") #adaptarse
        inicio.config(bg="#34495e", width = "1000", height = "768")

        #numero de variables
        tk.Label(inicio,text="Cantidad de variables: ",fg="#ecf0f1", bg="#34495e", font = ("Comic Sans MS",18)).place(x=10, y = 10)
        self.numvariables = tk.Entry(inicio,bg="#34495e",font = ("Comic Sans MS",18), fg="#bdc3c7", justify = tk.CENTER, width = 10, borderwidth=5)
        self.numvariables.place(x=270, y = 10)
        self.numvariables.insert(0,"Digite")
        self.numvariables.bind("<Button>",self.userText)

        #numero de restricciones
        tk.Label(inicio, text="Cantidad de restricciones: ", fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).place(x=10, y=80)
        self.numvarestricciones = tk.Entry(inicio,bg="#34495e",font = ("Comic Sans MS",18), fg="#bdc3c7", justify = tk.CENTER, width = 10, borderwidth=5)
        self.numvarestricciones.place(x=320, y=80)
        self.numvarestricciones.insert(0, "Digite")
        self.numvarestricciones.bind("<Button>",self.userText1)

        #Boton
        tk.Button(inicio,text="Continuar", fg = "#34495e", bg = "#95a5a6", font=("Comic Sans MS", 19), cursor = "hand2", command=self.validar).place(x=350,y=200)

        self.window.mainloop()

    def validar(self):
        estructuraentero = re.compile('^\d{1,2}$')
        if (re.search(estructuraentero, self.numvarestricciones.get()) is None) or (re.search(estructuraentero, self.numvariables.get()) is None):
            messagebox.showwarning("Cuidado", "Ingrese maximo 2 numeros entero")
        else:
            rest = self.numvariables.get()
            var = self.numvarestricciones.get()
            self.window.destroy()
            Pregunta(rest,var)

class Pregunta:
    def __init__(self,MaxVar,MaxRest):
        self.MaxVar = MaxVar
        self.MaxRest = MaxRest
        self.numvar = []
        self.numrest = []
        self.window = Tk()
        self.ventana2()
        self.inicio = None

    def ventana2(self):
        self.window.title("Metodo simplex")
        #self.window.resizable(0,0)  #No redimension
        w, h = self.window.winfo_screenwidth(), self.window.winfo_screenheight()
        self.window.geometry("%dx%d+0+0" % (w, h))
        self.window.iconbitmap("logo.ico")  # Logo
        scrollbar = Scrollbar(self.window, orient="horizontal")
        scrollbar2 = Scrollbar(self.window)
        c = Canvas(self.window,xscrollcommand=scrollbar.set,yscrollcommand=scrollbar2.set,bg="#34495e", width = "1000", height = "768")
        scrollbar.config(command=c.xview)
        scrollbar2.config(command=c.yview)
        scrollbar2.pack(side=RIGHT,fill=Y)
        scrollbar.pack(side=BOTTOM, fill=X)
        self.inicio = Frame(c,bg="#34495e") #el frame
        self.inicio.columnconfigure(0, weight=1)
        self.inicio.rowconfigure(0, weight=1)
        c.pack(side="left", fill="both", expand=True)
        c.create_window(0, 0, window=self.inicio, anchor="nw")


        #Maximizar o min
        labelF = Label(self.inicio, text="Funcion:", fg="#f1c40f", bg="#34495e", font=("Comic Sans MS", 18))
        labelF.grid(row=0,column=0,pady=30)

        funcion = ttk.Combobox(self.inicio,state="readonly", width = 5, values=["Max z", "Min z"],font = ("Comic Sans MS",18))
        funcion.set("Max z")
        funcion.grid(row=1, column=0)

        Label(self.inicio, text="=", fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=1, column=1)

        for i in range(int(self.MaxVar)):
            self.crearT(i)
            if i == 0: texto = "X"+str(i+1)
            else: texto += ",X"+str(i+1)
        Label(self.inicio, text=texto+"≥0", fg="#f1c40f", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=int(self.MaxRest)+5,column=0,pady=30)

        Label(self.inicio, text="Restricciones:", fg="#f1c40f", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=2,column=0,pady=30,padx=20)

        desig = []
        result = []
        for i in range(int(self.MaxRest)):
            for j in range(int(self.MaxVar)):
                self.crearR(i,j)
            desig.append(ttk.Combobox(self.inicio,state="readonly", width = 2, values=["≤", "≥","="],font = ("Comic Sans MS",18)))
            desig[i].set("≤")
            desig[i].grid(row=3+i, column = int(self.MaxVar)*4,padx=20)
            result.append(Entry(self.inicio, bg="#34495e", font=("Comic Sans MS", 18), fg="#f1c40f",justify=CENTER, width=10, borderwidth=5))
            result[i].grid(row=3+i, column = int(self.MaxVar)*5,padx=20)

        self.window.update()
        c.config(scrollregion=c.bbox("all"))

        boton = Button(self.inicio, text="Continuar", fg="#34495e", bg="#95a5a6", font=("Comic Sans MS", 19),cursor="hand2", command= lambda : self.pasar(funcion.get(), desig, result))
        boton.grid(row=int(self.MaxRest) + 6, column=1)

        boton2 = Button(self.inicio, text="Inicio", fg="#b8e994", bg="#b71540", font=("Comic Sans MS", 19),cursor="hand2", command=lambda: self.inicio1())
        boton2.grid(pady=30)

        self.window.mainloop()


    def inicio1(self):
        self.window.destroy()
        Main()

    def pasar(self,funcion,desig1,result):
        numvar = []
        numrest = []
        desig = []
        result1 = []
        for i in range(len(self.numvar)):
            numvar.append(self.numvar[i].get())

        for i in range(int(self.MaxRest)):
            numrest.append([])
            for j in range(int(self.MaxVar)):
                numrest[i].append(self.numrest[i][j].get())

        for i in range(len(desig1)):
            desig.append(desig1[i].get())

        for i in range(len(result)):
            result1.append(result[i].get())
        self.window.destroy()
        Resolver(funcion, numvar, numrest, desig, result1)
#---------------------------------------------
    def crearT(self,i):
        if(i==0):
            self.numvar.append(Entry(self.inicio, bg="#34495e", font=("Comic Sans MS", 18), fg="#f1c40f", justify=CENTER, width=10, borderwidth=5))
            self.numvar[i].grid(row=1, column=2)
            Label(self.inicio, text="X"+str(i+1), fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=1, column=3)
        else:
            Label(self.inicio, text="+", fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=1, column=i*3+1)
            self.numvar.append(Entry(self.inicio, bg="#34495e", font=("Comic Sans MS", 18), fg="#f1c40f",justify=CENTER, width=10, borderwidth=5))
            self.numvar[i].grid(row=1, column=i*3+2)
            Label(self.inicio, text="X" + str(i+1), fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=1, column=i*3+3)

    def crearR(self, i,j):
        self.numrest.append([])
        if(j==0):
            self.numrest[i].append(Entry(self.inicio, bg="#34495e", font=("Comic Sans MS", 18), fg="#f1c40f", justify=CENTER, width=10,borderwidth=5))
            self.numrest[i][j].grid(row=3+i, column=2, pady = 10)
            Label(self.inicio, text="X" + str(j + 1), fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=3+i, column=3)
        else:
            Label(self.inicio, text="+", fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=3+i, column=j*3+1)
            self.numrest[i].append(Entry(self.inicio, bg="#34495e", font=("Comic Sans MS", 18), fg="#f1c40f",justify=CENTER, width=10, borderwidth=5))
            self.numrest[i][j].grid(row=3+i,column=j*3+2)
            Label(self.inicio, text="X" + str(j + 1), fg="#ecf0f1", bg="#34495e", font=("Comic Sans MS", 18)).grid(row=3+i, column=j*3+3)


class Resolver:
    def __init__(self,funcion,variables,restricciones,desig,result):
        self.funcion = funcion
        self.variables = variables
        self.restricciones = restricciones
        self.desig = desig
        self.result = result
        self.window = Tk()
        self.matriz = []
        self.degenerado = []
        self.bandera = False
        self.bandera1 = False
        self.count = 0
        self.empezar()
        self.ventana2()
        self.boton = None
        self.inicio = None
        self.pivote = None

    def mostrar(self):
        print("funcion: ",self.funcion,"\n\n")

        for i in range(len(self.variables)):
            print("X%d="%i+str(self.variables[i])+"\t\t",end="")

        print("\n")
        for i in range(len(self.restricciones)):
            print("Restriccion %d" % (i+1))
            for j in range(len(self.restricciones[i])):
                print("X%d=" % (j) + str(self.restricciones[i][j])+"\t\t",end="")
            print("\n")

        for i in range(len(self.desig)):
            print("desigualdad %d" % i+str(self.desig[i])+"\t",end="")
        print("\n")

        for i in range(len(self.result)):
            print("Resultado %d" % i+str(self.result[i])+"\t",end="")
        print("\n")

    def ventana2(self):
        self.window.title("Metodo simplex")
        self.window.iconbitmap("logo.ico")  # Logo
        w, h = self.window.winfo_screenwidth(), self.window.winfo_screenheight()
        self.window.geometry("%dx%d+0+0" % (w, h))
        scrollbar = Scrollbar(self.window, orient="horizontal")
        scrollbar2 = Scrollbar(self.window)
        c = Canvas(self.window, xscrollcommand=scrollbar.set, yscrollcommand=scrollbar2.set, bg="#34495e", width="1000",height="768")
        scrollbar.config(command=c.xview)
        scrollbar2.config(command=c.yview)
        scrollbar2.pack(side=RIGHT, fill=Y)
        scrollbar.pack(side=BOTTOM, fill=X)
        self.inicio = Frame(c,bg="#34495e") #el frame
        self.inicio.columnconfigure(0, weight=1)
        self.inicio.rowconfigure(0, weight=1)
        c.pack(side="left", fill="both", expand=True)
        c.create_window(0, 0, window=self.inicio, anchor="nw")



        Label(self.inicio, width="4", height="2", bg="#34495e",font=("Comic Sans MS", 19)).grid(row=0, column=0, pady=50,padx=50)

        self.crearMatriz()

        self.boton = Button(self.inicio, text="Continuar", fg="#34495e", bg="#95a5a6", font=("Comic Sans MS", 19), cursor="hand2", command=lambda: self.obtenerPivote())
        self.boton.grid()

        self.boton2 = Button(self.inicio, text="inicio", fg="#b8e994", bg="#b71540", font=("Comic Sans MS", 19),cursor="hand2", command=lambda: self.inicio1())
        self.boton2.grid(pady=30)

        self.window.update()
        c.config(scrollregion=c.bbox("all"))
        self.window.mainloop()
    def inicio1(self):
        self.window.destroy()
        Main()

    def empezar(self):
        matriz1 = Tabla(self.funcion, self.variables, self.restricciones, self.desig, self.result)
        self.matriz = matriz1.matrizlol()

    def crearMatriz(self):
        label = []
        for i in range(4 + len(self.result)):
            label.append([])
            for j in range(len(self.matriz[0])):
                if self.matriz[i][j] == "":
                    label[i].append(Label(self.inicio, width="6", height="2", bg="#7f8c8d", font=("Comic Sans MS", 14),borderwidth=2, relief="solid"))
                    label[i][j].grid(row=i + 1, column=j + 1)
                else:
                    label[i].append(Label(self.inicio, text=self.matriz[i][j], fg="#d63031", width="6", height="2",bg="#95a5a6", font=("Comic Sans MS", 14), borderwidth=2, relief="solid"))
                    label[i][j].grid(row=i + 1, column=j + 1)

        pivote = CEspeciales(self.matriz, self.funcion)
        pivote.pivoteE()
        pivote.pivoteS()
        self.pivote = pivote.pivotefc
        if (pivote.nacotada):
            self.bandera1 = True
            self.boton.config(state='disabled')


        if pivote.degenerado:
            self.bandera = True


        if pivote.solucion:
            self.bandera = False
            self.boton.config(state = 'disabled')
            a = str(self.matriz[len(self.matriz)-2][2])
            mult = False
            si = False
            if(a.find("M")==-1):
                Label(self.inicio, text="Solucion = "+str(self.matriz[len(self.matriz)-2][2]), font=("Comic Sans MS", 19), bg="#34495e",fg="#fff200").grid(columnspan=3,row = 0 , column=1)
            else:
                messagebox.showwarning("Advertencia", "Problema no acotado hay variables artificiales en la solucion")
            for i in range(len(self.variables)):
                for j in range(2,len(self.matriz)-2):
                    if self.matriz[j][1].count("X"+str(i+1)) > 0:
                        Label(self.inicio, text="X%d = "%(i+1) + str(self.matriz[j][2]),font=("Comic Sans MS", 19), bg="#34495e", fg="#fff200").grid(columnspan=3, row=0, column=3+i*2)
                        si = True
                        break
                if si == False:
                    Label(self.inicio, text="X%d = 0"%(i+1),font=("Comic Sans MS", 19), bg="#34495e", fg="#fff200").grid(columnspan=3, row=0,column=3+i*2)
            for i in range(len(self.variables)):
                for j in range(2,len(self.matriz)-2):
                    if self.matriz[j][1].count("X"+str(i+1)) > 0:
                        mult = True
                if mult == False: break
            if(mult==False and self.bandera1 == False):
                messagebox.showwarning("Advertencia", "Es una solucion optima multiple")

        if (pivote.abierto):
            self.boton.config(state='disabled')
        else:
            for i in range(len(self.matriz[0])):
                label[pivote.pivotefc[1]][i].config(bg="#1abc9c")
            for i in range(len(self.matriz)):
                label[i][pivote.pivotefc[0]].config(bg="#1abc9c")
            label[pivote.pivotefc[1]][pivote.pivotefc[0]].config(bg="#8e44ad")

        if self.bandera:
            degenerado = []
            for i in range(len(self.matriz)):
                degenerado.append(str(self.matriz[i][2]))
            if(degenerado == self.degenerado):
                self.count += 1
                if(self.count == 3):
                    self.boton.config(state='disabled')
                    messagebox.showwarning("Advertencia", "Es un problema con ciclajes")
                    Label(self.inicio, text="Solucion = " + str(self.matriz[len(self.matriz) - 2][2]),font=("Comic Sans MS", 19), bg="#34495e").grid(columnspan=3, row=0, column=1)
            self.degenerado = degenerado

    def obtenerPivote(self):
        matriz = Matriz(self.matriz,self.pivote)
        self.matriz = matriz.convinar()
        self.crearMatriz()



# Inicia la aplicación
if __name__ == '__main__':    
    app = QApplication([])
    mi_App = MainWindow()
    mi_App.show()
    sys.exit(app.exec_())
